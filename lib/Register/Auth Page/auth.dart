import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shymbulak/actions/actions.dart';

import '../../main.dart';

class AuthPage extends StatefulWidget {
  AuthPage({Key? key}) : super(key: key);

  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();

  bool _passwordVisible = false;
  @override
  void initState() {
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(94, 148, 225, 1),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 147, left: 30, right: 20),
                width: 400,
                child: Text('Добро пожаловать,\nАвторизуйтесь',
                    style: GoogleFonts.lato(
                        letterSpacing: 0.5,
                        height: 1.3,
                        fontSize: 32,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        color: Colors.white)),
              ),
              Container(
                margin: EdgeInsets.only(top: 67, bottom: 12),
                height: 68,
                width: 327,
                child: TextFormField(
                  controller: email,
                  style: GoogleFonts.lato(
                      letterSpacing: 0.5,
                      height: 1.3,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      color: Colors.white),
                  autofocus: false,
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15.0),
                          ),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 0.4),
                          )),
                      hintText: 'E-mail',
                      hintStyle:
                          TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.8)),
                          borderRadius: BorderRadius.circular(15)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.4)),
                          borderRadius: BorderRadius.circular(15))),
                ),
              ),
              Container(
                height: 68,
                width: 327,
                child: TextFormField(
                  controller: password,
                  style: GoogleFonts.lato(
                      letterSpacing: 0.8,
                      height: 1.3,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      color: Colors.white),
                  autofocus: false,
                  obscureText: !_passwordVisible,
                  obscuringCharacter: '●',
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15.0),
                          ),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 0.4),
                          )),
                      hintText: 'Пароль',
                      hintStyle:
                          TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.8)),
                          borderRadius: BorderRadius.circular(15)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.4)),
                          borderRadius: BorderRadius.circular(15))),
                ),
              ),
              Container(
                width: 327,
                height: 58,
                margin: EdgeInsets.only(top: 12),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Color.fromRGBO(255, 255, 255, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(15.0),
                      )),
                  child: Text(
                    'Войти',
                    style: GoogleFonts.lato(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        color: Color.fromRGBO(94, 148, 225, 1)),
                  ),
                  onPressed: () => loginRequest(),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 16, right: 35),
                    child: Text('Регистрация',
                        style: GoogleFonts.lato(
                            letterSpacing: 0.5,
                            height: 1.3,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Colors.white)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Text('Забыли пароль?',
                        style: GoogleFonts.lato(
                            letterSpacing: 0.5,
                            height: 1.3,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Color.fromRGBO(255, 255, 255, 0.4))),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 24, bottom: 16),
                child: Text('Или войдите через:',
                    style: GoogleFonts.lato(
                        letterSpacing: 0.5,
                        height: 1.3,
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        color: Color.fromRGBO(255, 255, 255, 1))),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: const Icon(Icons.emoji_emotions_rounded),
                    iconSize: 42,
                    color: Colors.white,
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: const Icon(Icons.emoji_events_rounded),
                    iconSize: 42,
                    color: Colors.white,
                    onPressed: () {},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  loginRequest() async {
    final device_token =
        (await SharedPreferences.getInstance()).get('device_token');
    print(device_token);
    print('$apiEndPoint/auth/login');

    final response = await post(Uri.parse('$apiEndPoint/auth/login'), headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    }, body: {
      "email": email.text,
      "password": password.text,
    });
    final body = jsonDecode(response.body);
    print(response.body);
    print(response.statusCode);
    if (response.statusCode == 200) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['token']);
      localStorage.setString('user', jsonEncode(body['user']));
      var userJson = localStorage.getString('user');
      // ignore: unused_local_variable
      var user = json.decode(userJson!);
      Navigator.push(
          context, new MaterialPageRoute(builder: (context) => ActionsPage()));
    } else {
      Text('fuck this world');
    }
  }
}
