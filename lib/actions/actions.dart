import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ActionsPage extends StatefulWidget {
  ActionsPage({Key? key}) : super(key: key);

  @override
  _ActionsPageState createState() => _ActionsPageState();
}

class _ActionsPageState extends State<ActionsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.blue),
              onPressed: () {},
            );
          },
        ),
        backgroundColor: Colors.white,
        title: Text(
          'Активности',
          style: GoogleFonts.lato(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
              color: Colors.black),
        ),
        elevation: 0,
      ),
      body: Center(),
    );
  }
}
