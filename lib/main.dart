import 'package:flutter/material.dart';
import 'package:shymbulak/Register/Auth%20Page/auth.dart';

void main() {
  runApp(MyApp());
}

final apiEndPoint = 'https://api.shymbulak-dev.com/user-service';

class MyApp extends StatelessWidget {
  // This widget is the root of your applcation.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: AuthPage(),
    );
  }
}
